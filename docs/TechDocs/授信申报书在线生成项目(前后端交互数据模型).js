var Business = {
    apply_amount: Number
  , apply_type: String
  , apply_expire: Number
  , main_manager: String
  , secondary_manager: String
  , operate_manager: String
  , apply_manager: String
  , apply_date: Date
  , company: {
      name: String
    , credit: String
    , legal_represent: String
    , actual_controller: String
    , actual_controller_introduction: String
    , establish_time: Date
    , register_cash: Number
    , actual_cash: Number
    , operate_range: String
    , operate_cert_expire: String
    , organ_cert_expire: String
    , is_cert_verify: Boolean
    , industry: String
    , mtype: String
    , size: String
    , client_type: String
    , cash_source: String
    , operate_address: String
    , operate_address_ownership: String
    , operate_address_area: Number
    , operate_address_value: Number
    , is_operate_address_pledge: Boolean
    , operate_address_type: String
    , holder_count: Number
    , staff_count: Number
    , holders: [{
	name: String
      , cert_number: String
      , amount: Number
      , mtype: String
      , rate: Number
      , main_business: String
      , phone: String
    }]
    , managers: [{
	name: String
      , position: String
      , age: String
      , education: String
      , title: String
      , expire: Number
      , start: Date
    }]
    , staff_structure: {
	total: Number
      , product: Number
      , technology: Number
      , sale: Number
      , manager: Number
      , master: Number
      , university: Number
      , college: Number
      , vocation: Number
    }
    , relate_companys: [{
	is_verify: Boolean
      , name: String
      , register_capital: Number
      , amount: Number
      , mtype: String
      , is_other_holding: Boolean
      , holder: String
      , holder_controller_relation: String
      , rate: Number
      , remark: String
      , main_bussiness: String
      , current_asset: Number
      , sale_income: Number
      , current_debt: Number
      , profit: Number
      , total_asset: Number
      , total_debt: Number
      , time: Date
      , holders: [{
          name: String
        , amount: Number
        , mtype: String
        , rate: Number
        , main_bussiness: String
      }]
    }]
    , competitors: [{
	name: String
      , product: String
      , brand: String
      , local_rate: Number
      , industry_rate: Number
      , advantage: String
      , disadvantage: String
    }]
    , partners: [{
	mtype: Number
      , name: String
      , product: String
      , expire: Number
      , trade_amount: Number
      , payable: Number
      , average_expire: Number
      , total_rate: Number
      , settle_type: String
    }]
    , assured_objects: [{
	number: Number
      , amount: Number
      , borrower: String
      , mtype: String
      , over: Number
      , expose: Number
    }]
    , bank_credits: [{
	name: String
      , amount: Number
      , long_over: Number
      , short_over: Number
      , over: Number
      , deposit: Number
      , expose: Number
      , assure: Number
      , time: Date
    }]
    , asset_debts: [{
	report_time: Date
      , report_type: String
      , cash: Number
      , short_invest: Number
      , receive_bill: Number
      , receivable: Number
      , pre_pay: Number
      , other_receivable: Number
      , inventory: Number
      , await_receivable: Number
      , await_circle_lost: Number
      , expire_asset: Number
      , other_circle_asset: Number
      , attached_company_between: Number
      , long_invest: Number
      , fixed_origin: Number
      , fixed_lost: Number
      , fixed_real: Number
      , fixed_price: Number
      , fixed_repair: Number
      , current_build: Number
      , await_fixed_lost: Number
      , invisible_asset: Number
      , long_wait_pay: Number
      , defer_tax: Number
      , short_debt: Number
      , pay_bill: Number
      , payable: Number
      , pre_receive: Number
      , other_pay: Number
      , pay_wage: Number
      , pay_welfare: Number
      , pay_tax: Number
      , pay_profit: Number
      , other_payable: Number
      , await_payable: Number
      , guess_debt: Number
      , expire_debt: Number
      , other_circle_debt: Number
      , long_debt: Number
      , pay_bond: Number
      , long_payable: Number
      , other_long_debt: Number
      , receive_cash: Number
      , addition_cash: Number
      , surplus_cash: Number
    }]
    , profits: [{
	report_time: Date
      , report_type: String
      , main_income: Number
      , main_cost: Number
      , main_addition: Number
      , other_profit: Number
      , operate_cost: Number
      , manage_cost: Number
      , finance_cost: Number
      , invest_income: Number
      , subsidy_income: Number
      , other_income: Number
      , other_cost: Number
      , income_tax: Number
    }]
    , cash_flows: [{
	report_time: Date
      , report_type: String
      , operate_inflow: Number
      , tax_inflow: Number
      , other_operate_inflow: Number
      , operate_outflow: Number
      , staff_outflow: Number
      , tax_outflow: Number
      , other_operate_outflow: Number
      , cancel_invest_inflow: Number
      , invest_inflow: Number
      , asset_inflow: Number
      , other_invest_inflow: Number
      , asset_outflow: Number
      , invest_outflow: Number
      , bond_outflow: Number
      , other_invest_outflow: Number
      , right_invest_inflow: Number
      , bond_inflow: Number
      , debt_inflow: Number
      , other_financing_inflow: Number
      , debt_outflow: Number
      , financing_outflow: Number
      , allocate_profit_outflow: Number
      , interest_outflow: Number
      , lease_outflow: Number
      , cut_cash_outflow: Number
      , other_financing_outflow: Number
      , exchange_include: Number
      , increase_cash: Number
      , fixed_asset_lost: Number
      , invisible_asset_amort: Number
      , long_amort: Number
      , cut_amort: Number
      , increase_pre_debt: Number
      , other_asset_lost: Number
      , fixed_asset_scrap: Number
      , finance_outflow: Number
      , invest_lost: Number
      , defer_tax: Number
      , cut_inventory: Number
      , cut_operate_inflow: Number
      , increase_operate_outflow: Number
      , increase_tax: Number
      , cash_end: Number
      , cash_start: Number
      , other_cash_end: Number
      , other_cash_start: Number
    }]
    , bank_accounts: [{
	mtype: String
      , name: String
      , account: String
      , over: Number
      , deposit: Number
    }]
    , bills: [{
	mtype: String
      , amount: Number
      , remark: String
      , detail: [{
	  response: String
	, amount: Number
	, create_date: Date
	, expire_date: Date
	, phone: String
	, relation: String
	, previous_owner: String
      }]
    }]
    , receivables: [{
	name: String
      , phone: String
      , sale: Number
      , asset: Number
      , reason: String
      , amount: Number
      , expire: Number
    }]
    , inventorys: [{
	name:String
      , mtype: Number
      , cost_price: Number
      , market_price: Number
      , purchase_count: Number
      , count: Number
      , address: String
      , is_pledge: Boolean
      , sale_target: String
    }]
    , fixed_assets: [{
	name: String
      , origin_value: Number
      , book_value: Number
      , cert_number: String
      , owner: String
      , pledge: String
      , address: String
    }]
    , long_invests: [{
	mtype: String
      , name: String
      , total_invest: Number
      , self_invest: Number
      , expire: Number
      , income: Number
    }]
    , bank_settles: [{
	number: String
      , name: String
      , borrow_count: Number
      , borrow_amount: Number
      , lend_coumt: Number
      , lend_amount: Number
    }]
    , program: {
	/*字段同business.program一样, 无需填写.*/
    }
    , analyze: {
        industry: String
      , history: String
      , honor: String
      , advantage: String
      , market_expand: String
      , industry_chain_position: String
      , industry_position: String
      , program_change: String
      , abnormal_detail: String
      , assure_object_datail: String
      , actual_controller_introduction: String
      , cash_change: String
      , bill_change: String
      , receivable_change: String
      , inventory_change: String
      , fixed_asset_change: String
      , flow_rate: String
      , quick_rate: String
      , debt_rate: String
      , income_rate: String
      , interest_time: String
      , debt_repay_rate: String
      , asset_profit_rate: String
      , sale_profit_rate: String
      , cost_profit_rate: String
      , cash_flow: String
      , finance: String
      , credit_detail: String
      , our_bank_competitive: String
      , long_invest_change: String
    }
  }
  , program: {
      assure_type: String
    , expire: Date
    , amount: Number
    , status: String
    , expose: Number
    , limit_cycle: Boolean
    , regulate: Boolean
    , assure_type: String
    , total_average: Number
    , settle_average: Number
    , deposit_average: Number
    , mtype: String
    , count: Number
    , start: Date
    , end: Date
    , deposit: Number
    , risk: Number
    , remark: String
    , operation_type: String
    , pawns: [{
	mtype: 'rent'
      , name: String
      , owner: String
      , relation: String
      , origin_price: Number
      , current_price: Number
      , assure_cash: Number
      , number: String
      , address: String
      , use: String
      , is_other_bank_pledge: String
      , is_verify: Number
      , cost: Number
      , response: String
      , value: Number
      , expose_rate: String
      , evaluate_type: String
    }, {
	mtype: 'contract'
      , name: String
      , owner: String
      , relation: String
      , origin_price: Number
      , current_price: Number
      , assure_cash: Number
      , number: String
      , debtor: String
      , amount: Number
      , expire: Number
      , is_other_bank_pledge: Boolean
    }, {
	mtype: 'brand'
      , name: String
      , owner: String
      , relation: String
      , origin_price: Number
      , current_price: Number
      , assure_cash: Number
      , number: Number
      , register_department: String
      , expire: Number
      , over_expire: Number
      , value: Number
      , evaluate_company: String
    }, {
	mtype: 'bill'
      , name: String
      , owner: String
      , relation: String
      , origin_price: Number
      , current_price: Number
      , assure_cash: Number
      , number: String
      , bank: String
      , amount: Number
      , expire: Number
      , is_other_bank_pledge: Boolean
    }, {
	mtype: 'bond'
      , name: String
      , owner: String
      , relation: String
      , origin_price: Number
      , current_price: Number
      , assure_cash: Number
      , issuer: String
      , is_transfer: Number
      , amount: Number
      , expire_date: Date
      , is_other_bank_pledge: Boolean
    }, {
	mtype: 'lading'
      , name: String
      , owner: String
      , relation: String
      , origin_price: Number
      , current_price: Number
      , assure_cash: Number
      , issuer: String
      , amount: Number
      , is_other_bank_pledge: Boolean
    }, {
	mtype: 'stock'
      , name: String
      , owner: String
      , relation: String
      , origin_price: Number
      , current_price: Number
      , assure_cash: Number
      , is_lised: Number
      , number: String
      , current_value: Number
      , value: Number
    }]
    , varieties: [{
	mtype: String
      , limit: Number
      , expose: Number
      , price: Number
      , deposit: Number
      , remark: String
      , expire: Number
      , interest_rate: Number
    }]
    , analyze: {
        rent: String
      , rent_repay: String
      , rent_price: String
      , rent_operate: String
      , contract: String
      , contract_repay: String
      , contract_price: String
      , contract_operate: String
      , bill: String
      , bond: String
      , lading: String
      , lading_operate: String
      , brand: String
      , stock: String
      , other_pledge: String
      , assure: String
      , other_assure: String
      , use: String
      , reasonable: String
      , operate_detail: String
      , income: String
      , bill_income: String
      , interest_income: String
      , deposit_income: String
      , settle_income: String
      , formality_income: String
      , other_income: String
      , summary: String
      , assure_pawn: String
      , risk_prevent: String
      , other_risk: String
      , operation_detail: String
    }
    , pledges: [{
	mtype: 'building'
      , name: String
      , owner: String
      , relation: String
      , cert_number: String
      , address: String
      , cost: Number
      , value: Number
      , evaluate_company: String
      , lost_rate: Number
      , use_duration: Number
      , over_duration: Number
      , expose_rate: Number
      , is_only: Number
      , rent_rate: Number
      , average_rent_price: Number
      , market_rent_price: Number
      , market_sale_price: Number
      , land_area: Number
      , house_area: Number
      , land_area: Number
      , house_area: Number
    }]
  }
  , assures: [{
      mtype: String
    , name: String
    , is_holder: Boolean
    , age: Number
    , education: String
    , title: String
    , abnormal_detail: String
    , ability: String
    , assets: [{
	mtype: String
      , owner: String
      , owner_cert: String
      , address: String
      , is_pledge: Boolean
      , market_price: Number
    }]
    , assured_objects: [{
	number: String
      , amount: String
      , borrower: String
      , mtype: String
      , over: Number
      , expose: Number
    }]
    , bank_settles: [{
	number: String
      , name: String
      , borrow_count: Number
      , bowwow_amount: Number
      , lend_count:Number
      , lend_amount: Number
    }]
  }, {
      /*拥有和company一样的字段*/
  }]
}