﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Generate.Tool;

namespace Generate.Model
{
    public class Business
    {
        public const string COLL_NAME = "businesses";
        public static MongoCollection<BsonDocument> coll = Mongo.db.GetCollection<BsonDocument>(COLL_NAME);
        public BsonDocument entity = null;

        public Business(string id)
        {
            var objectId = ObjectId.Parse(id);
            var business = coll.FindOneById(objectId);
            //var business = coll.FindOne();
            this.entity = business;
        }

        public static BsonDocument Get(string id)
        {
            //FIXME: For Test.
            //var business = coll.FindOneById(id);
            var business = coll.FindOne();
            return business;
        }

        public static List<BsonDocument> All()
        {
            var cursor = coll.FindAll();
            var businesses = new List<BsonDocument>();

            foreach(var business in cursor)
            {
                businesses.Add(business);
            }

            return businesses;
        }

        public bool IsExist(string fullname)
        {
            BsonValue value = this.GetValue(fullname);
            bool result = true;

            if(value == null)
            {
                result = false;
            }
            else if (value.BsonType == BsonType.Document)
            {
                if(value.ToString().Length == 0)
                {
                    result = false;
                }
            }
            else if (value.BsonType == BsonType.Array)
            {
                int l = value.AsBsonArray.ToArray().Length;
                if(l == 0)
                {
                    result = false;
                }
            }

            return result;
        }

        public int GetValueLength(string fullname)
        {
            int l = 0;
            BsonValue value = this.GetValue(fullname);
            if(value != null && value.BsonType == BsonType.Array)
            {
                l = value.AsBsonArray.Count;
            }
            return l;
        }

        public BsonValue GetValue(string fullname)
        {
            Console.WriteLine(fullname);
            var names = fullname.Split('.').Skip(1).ToArray();

            BsonValue value = null;
            //Console.WriteLine(fullname);
            for (int i = 0, l = names.Length; i < l; i++ )
            {
                var name = names[i];
                //Console.WriteLine(name);
                if(i == 0)
                {
                    //Console.WriteLine(this.entity.GetValue("_id"));
                    value = this.entity.GetValue(names[0]);
                }
                else
                {
                    if (value.BsonType == BsonType.Document)
                    {
                        try
                        {
                            value = value.AsBsonDocument.GetValue(name);//null
                        }
                        catch (KeyNotFoundException e)
                        {
                            value = null;
                        }
                        if (value == null)
                        {
                            return value;
                        }
                    }else if (value.BsonType == BsonType.Array)
                    {
                        if (name.Contains("="))
                        {
                            var kv = name.Split('=');
                            var k = kv[0];
                            var v = kv[1];
                            var arr = value.AsBsonArray;
                            BsonArray newArr = new BsonArray();
                            foreach(var e in arr)
                            {
                                try
                                {
                                    if (e.AsBsonDocument.GetValue(k) + "" == v)
                                    {
                                        newArr.Add(e);
                                    }
                                }
                                catch (KeyNotFoundException err)
                                {
                                }
                            }
                            value = newArr;
                        }
                        else
                        {
                            int index = int.Parse(name);
                            try
                            {
                                value = value.AsBsonArray[index];
                            }
                            catch (ArgumentOutOfRangeException e)
                            {
                                value = null;
                            }
                            if (value == null)
                            {
                                return value;
                            }
                        }
                    }
                }
            }

            if (value + "" == "BsonNull")
            {
                value = null;
            }
            return value;
        }

        public static void Test()
        {
            var business = new Business("");
            var result = business.GetValue("name.0.b.c");
            Console.WriteLine(result);
            Console.Read();
        }
    }
}
