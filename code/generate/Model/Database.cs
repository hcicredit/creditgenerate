﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Generate.Model
{
    public class Database
    {
        /* TODO: Move these to configure file. */
        public const string SERVER = "localhost";
        public const string DBNAME = "credit_generate";
        public const string UID = "root";
        public const string PASSWORD = "msnhaosdent";

        public const string CONNECT_STRING = "SERVER=" + SERVER + ";"
                                                  + "DATABASE=" + DBNAME + ";"
                                                  + "UID=" + UID + ";"
                                                  + "PASSWORD=" + PASSWORD + ";";

        public const string ADD = "INSERT INTO $table SET $modifier";
        public const string DEL = "DELETE FROM $table $selector";
        public const string GET = "SELECT * FROM $table $selector";
        public const string UPDATE = "UPDATE $table SET $modifier $selector";

        private MySqlConnection connection = null;

        private string Convert(Dictionary<string, object> cmd)
        {
            string act = cmd["act"] + "";
            Dictionary<string, string> selector = (Dictionary<string, string>)cmd["selector"];
            Dictionary<string, string> modifier = (Dictionary<string, string>)cmd["modifier"];

            string selectorSql = "", modifierSql = "";
            foreach (var k in selector.Keys)
            {
                selectorSql += k + "=" + selector[k] + " AND ";
            }
            if (selectorSql.Length != 0)
            {
                selectorSql = "WHERE " + selectorSql;
                selectorSql = selectorSql.Substring(0, selectorSql.Length - 5);
            }
            foreach (var k in modifier.Keys)
            {
                modifierSql += k + "='" + modifier[k] + "', ";
            }
            if (modifierSql.Length != 0)
            {
                modifierSql = modifierSql.Substring(0, modifierSql.Length - 2);
            }

            string sql = act.Replace("$table", selector["table"] + "")
                            .Replace("$selector", selectorSql)
                            .Replace("$modifier", modifierSql);
            return sql;
        }

        public void Init()
        {
            connection = new MySqlConnection(CONNECT_STRING);
        }

        public void Open()
        {
            try
            {
                connection.Open();
            }
            catch (MySqlException ex)
            {
                Console.Write(ex + "");
            };
        }

        public void Close()
        {
            try
            {
                connection.Close();
            }
            catch (MySqlException ex)
            {
                Console.Write(ex + "");
            };
        }

        public void Query(Dictionary<string, object> cmd)
        {
            string sql = this.Convert(cmd);
            MySqlCommand order = new MySqlCommand(sql, connection);
            MySqlDataReader reader = null;
            if (cmd["act"] != GET)
            {
                order.ExecuteNonQuery();
            }
            else
            {
                reader = order.ExecuteReader();
            }
            
        }
    }
}
