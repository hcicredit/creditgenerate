﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SocketIOClient;

namespace Generate.controller
{
    public class Socket
    {
        public Client entity;

        public void Run()
        {
            Console.WriteLine("Start Client Socket......");
            entity = new Client("http://localhost:3000");
            entity.Opened += SocketOpened;
            entity.Message += SocketMessage;
            entity.SocketConnectionClosed += SocketConnectionClosed;
            entity.Error += SocketError;

            entity.On("connect", (fn) => 
            {
                Console.WriteLine("Connect Server Socket successfully......");
            });

            entity.On("fromforward", (args) =>
            {
                var data = args.Json.Args[0];
                var act = data["act"];
                if(act == "generate")
                {
                    var id = data["id"].Value;
                    Console.WriteLine("Receive Job:" + id);

                    Job job = new Job();
                    string businessId = id;
                    job.Run(businessId);

                    Console.WriteLine("Complete Job: " + id);

                    var cmdStr = "{\"act\": \"result\", \"id\": \"" + id + "\"}";
                    var cmd = JObject.Parse(cmdStr);
                    entity.Emit("forward", cmd);
                }
            });


            entity.Connect();
        }

        void SocketOpened(object sender, EventArgs e)
        {
        }

        void SocketConnectionClosed(object sender, EventArgs e)
        {
            Console.WriteLine("Server Socket closed.");
        }

        void SocketError(object sender, ErrorEventArgs e)
        {
            Console.WriteLine("Client Socket error:" + e.Message);
        }

        void SocketMessage(object sender, MessageEventArgs e)
        {
        }

        public void Stop()
        {
            if(this.entity != null)
            {
                entity.Opened -= SocketOpened;
                entity.Message -= SocketMessage;
                entity.SocketConnectionClosed -= SocketConnectionClosed;
                entity.Error -= SocketError;
                this.entity.Dispose();
            }
        }
    }
}
