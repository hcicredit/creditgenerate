﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using OpenXmlPowerTools;
using Generate.Tool;
using Generate.Properties;
using MongoDB.Bson;
using System.Xml;
using System.IO.Packaging;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Generate.Model;
using System.IO;
using Generate.Controller;


namespace Generate.controller
{
    public class Job
    {
        public void Run(string businessId)
        {
            Business business = new Business(businessId);
            string templatePath = Settings.Default.TemplateDir + "template.docx";
            string outputPath = Settings.Default.OutputDir + businessId + ".docx";
            File.Copy(templatePath, outputPath, true);
            using (WordprocessingDocument template = WordprocessingDocument.Open(outputPath, true))
            {
                var combiner = new Combiner();
                var filler = new Filler();
                combiner.Combine(template, business);
                filler.Fill(template, business);
            }
        }
    }
}
