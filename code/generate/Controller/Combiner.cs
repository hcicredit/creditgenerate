﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using OpenXmlPowerTools;
using Generate.Tool;
using Generate.Properties;
using MongoDB.Bson;
using System.Xml;
using System.IO.Packaging;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Generate.Model;

namespace Generate.Controller
{
    public class Combiner
    {
        public WordprocessingDocument Combine(WordprocessingDocument template, Business business)
        {
            this.MergeWR(template);
            this.CombineParts(business, template);
            this.CombineRows(business, template);
            return template;
        }

        public void Test()
        {
            string templatePath = Settings.Default.TemplateDir + "template.docx";
            string outputPath = Settings.Default.OutputDir + "result.docx";
            File.Copy(templatePath, outputPath, true);
            using(WordprocessingDocument template = WordprocessingDocument.Open(outputPath, true))
            {
                this.MergeWR(template);
                string mark = "ex.add.businesses.company";
                this.AddParts(template, mark, 2, 3);
                Console.Read();
                //this.AddRows(template, mark, 3, 4);
                //this.DeleteParts(template, mark, 3);
                //this.GetExistMarks(template);
                //this.GetArrNames(template);
                //this.DeleteParts(template, null);
                //this.AddRows(template, null);
            }
        }

        public WordprocessingDocument CombineParts(Business business, WordprocessingDocument template)
        {
            this.DeleteParts(template, business);
            this.AddParts(template, business);
            return template;
        }

        public WordprocessingDocument CombineRows(Business business, WordprocessingDocument template)
        {
            this.AddRows(template, business);
            return template;
        }

        public WordprocessingDocument MergeWR(WordprocessingDocument template)
        {
            XDocument templateXml = template.MainDocumentPart.GetXDocument();
            var pNodes = templateXml.Descendants(W.p)
                .Where(d => (d.Descendants(W.r).Count() > 1) && (d.Descendants(W.hyperlink).Count() == 0));
            foreach(var pNode in pNodes)
            {
                var rNodes = pNode.Descendants(W.r)
                    .ToArray();
                var result = "";
                XElement resultNode = null;
                foreach(var rNode in rNodes)
                {
                    var tNode = rNode.Descendants(W.t).FirstOrDefault();
                    if (tNode != null)
                    {
                        result += tNode.Value;
                        if(resultNode == null)
                        {
                            resultNode = new XElement(rNode);
                            //Console.WriteLine(rNode);
                        }
                        rNode.Remove();
                    }
                };
                if(resultNode != null)
                {
                    //Console.WriteLine(resultNode);
                    resultNode.Descendants(W.t).FirstOrDefault().Value = result;
                    pNode.Add(resultNode);
                }
                //Console.WriteLine(result);
            }
            template.MainDocumentPart.PutXDocument();
            return template;
        }

        public string[] GetExistMarks(WordprocessingDocument template)
        {
            string pattern = @"ex\.exist\S*";
            return this.GetMarks(template, pattern);
        }

        public string[] GetAddMarks(WordprocessingDocument template)
        {
            string pattern = @"ex\.add\S*";
            return this.GetMarks(template, pattern);
        }

        public string[] GetArrNames(WordprocessingDocument template)
        {
            string pattern = @"arr";
            return this.GetMarks(template, pattern);
        }

        public string[] GetMarks(WordprocessingDocument template, string pattern)
        {
            List<string> marks = new List<string>();
            XDocument templateXml = template.MainDocumentPart.GetXDocument();
            var markNodes = templateXml.Descendants(W.t)
                .Where(d => Regex.IsMatch(d.Value, pattern))
                .Select(d =>
                {
                    var markNode = d.Parent.Parent;
                    return markNode;
                });
            foreach (var markNode in markNodes)
            {
                marks.Add(markNode.Value);
                //Console.WriteLine(markNode.Value);
            }
            return marks.ToArray();
        }

        public WordprocessingDocument DeleteParts(WordprocessingDocument template, Business business)
        {
            var marks = this.GetExistMarks(template);
            int[] numbers = new int[marks.Length];
            string pattern = @"ex.exist.(\d+).(\S+)";
            for (int i = 0, l = marks.Length; i < l; i++)
            {
                string mark = marks[i];
                Match match = Regex.Match(mark, pattern);
                numbers[i] = int.Parse(match.Groups[1] + "");
                string fullname = match.Groups[2] + "";

                var isExist = business.IsExist(fullname);
                if (isExist)
                {
                    numbers[i] = 0;
                }
            }

            this.DeleteParts(template, marks, numbers);
            return template;
        }

        public WordprocessingDocument DeleteParts(WordprocessingDocument template, string[] marks, int[] numbers)
        {
            for(int i = 0, l = marks.Length; i < l; i++)
            {
                this.DeleteParts(template, marks[i], numbers[i]);
            }
            return template;
        }

        public WordprocessingDocument DeleteParts(WordprocessingDocument template, string mark, int number)
        {
            XDocument templateXml = template.MainDocumentPart.GetXDocument();
            List<XNode> markNodeList = new List<XNode>();

            var markNodes = templateXml.Descendants(W.t)
                .Where(d => (string)d.Value == mark)
                .Select(d =>
                {
                    var markNode = d.Parent.Parent;
                    return markNode;
                });

            foreach(var markNode in markNodes)
            {
                markNodeList.Add(markNode);
            }
            foreach(var markNode in markNodeList)
            {
                var currentMarkNode = markNode;
                var preMarkNode = markNode;
                for (int i = 0; i <= number; i++ )
                {
                    currentMarkNode = currentMarkNode.NextNode;
                    preMarkNode.Remove();
                    preMarkNode = currentMarkNode;
                }
            }

            template.MainDocumentPart.PutXDocument();
            return template;
        }

        public WordprocessingDocument AddParts(WordprocessingDocument template, Business business)
        {
            var marks = this.GetAddMarks(template);
            int[] numbers = new int[marks.Length];
            int[] lines = new int[marks.Length];
            string pattern = @"ex.add.(\d+).(\S+)";
            for (int i = 0, l = marks.Length; i < l; i++)
            {
                string mark = marks[i];
                Match match = Regex.Match(mark, pattern);
                lines[i] = int.Parse(match.Groups[1] + "");
                string arrPrefix = match.Groups[2] + "";
                numbers[i] = business.GetValueLength(arrPrefix);
            }

            this.AddParts(template, marks, lines, numbers);
            return template;
        }

        public WordprocessingDocument AddParts(WordprocessingDocument template, string[] marks, int[] lines, int[] numbers)
        {
            for (int i = 0, l = marks.Length; i < l; i++)
            {
                this.AddParts(template, marks[i], lines[i], numbers[i]);
            }
            return template;
        }

        public WordprocessingDocument AddParts(WordprocessingDocument template, string mark, int line, int number)
        {
            XDocument templateXml = template.MainDocumentPart.GetXDocument();
            List<XNode> markNodeList = new List<XNode>();

            var markNodes = templateXml.Descendants(W.t)
                .Where(d => (string)d.Value == mark)
                .Select(d =>
                {
                    var markNode = d.Parent.Parent;
                    return markNode;
                });

            foreach (var markNode in markNodes)
            {
                markNodeList.Add(markNode);
            }
            foreach (var markNode in markNodeList)
            {
                XNode[] addNodes = new XNode[line];
                var currentNode = markNode;
                for (int i = 0; i < line; i++)
                {
                    currentNode = currentNode.NextNode;
                    addNodes[i] = currentNode;
                }
                markNode.Remove();
                for (int j = number - 1; j >=0; j-- )
                {
                    for (int i = line - 1; i >= 0; i--)
                    {
                        currentNode.AddAfterSelf(addNodes[i]);
                    }
                    var tempNode = (XElement)currentNode;
                    for (int i = 0; i < line; i++)
                    {
                        tempNode = (XElement)tempNode.NextNode;
                        var pattern = @"\.\d*arr";
                        Regex regex = new Regex(pattern);
                        if (tempNode.Descendants(W.t).Count() != 0)
                        {
                            var cells = tempNode.Descendants(W.t)
                                .Where(d => Regex.IsMatch(d.Value, pattern));
                            foreach (XElement cell in cells)
                            {
                                cell.Value = regex.Replace(cell.Value, "." + j, 1);
                            }
                        }
                    }
                }
                for (int i = 0; i < line; i++ )
                {
                    var tempNode = currentNode;
                    currentNode = currentNode.PreviousNode;
                    tempNode.Remove();
                }
            }

            template.MainDocumentPart.PutXDocument();
            return template;
        }

        public WordprocessingDocument AddRows(WordprocessingDocument template, Business business)
        {
            var names = this.GetArrNames(template);
            string linePattern = @"\S+\.(\d+)arr\.\S+";
            string namePattern = @"(\S+)\.\d*arr\.\S+";
            for (int i = 0, l = names.Length; i < l; i++ )
            {
                var name = names[i];
                Match match = Regex.Match(name, linePattern);
                int line = 1;
                if (match.Groups.Count > 1)
                {
                    line = int.Parse(match.Groups[1] + "");
                }
                match = Regex.Match(name, namePattern);
                string arrPrefix = match.Groups[1] + "";
                int number = business.GetValueLength(arrPrefix);
                this.AddRows(template, name, line, number);
            }
            return template;
        }

        public WordprocessingDocument AddRows(WordprocessingDocument template, string mark, int line, int number)
        {
            XDocument templateXml = template.MainDocumentPart.GetXDocument();
            var isExist = templateXml.Descendants(W.t).FirstOrDefault(d => d.Value == mark) != null;
            if (!isExist)
            {
                return null;
            }
            XElement markRow = templateXml.Descendants(W.t)
                .FirstOrDefault(d => d.Value == mark)
                .Parent.Parent.Parent.Parent;
            XElement tempRow;

            string pattern = @"\.\d*arr";
            Regex regex = new Regex(pattern);
            for (var i = 0; i < number; i++ )
            {
                tempRow = markRow;
                XElement[] insertRows = new XElement[line];
                for (var j = 0; j < line; j++)
                {
                    insertRows[j] = new XElement(markRow);
                    tempRow = (XElement)tempRow.NextNode;
                }

                var cells = insertRows.Descendants(W.t)
                    .Where(d => Regex.IsMatch(d.Value, pattern));
                foreach (XElement cell in cells)
                {
                    cell.Value = regex.Replace(cell.Value, "." + (number - i - 1), 1);
                }
                markRow.AddAfterSelf(insertRows);
            }
            markRow = templateXml.Descendants(W.t)
                .FirstOrDefault(d => (string)d.Value == mark)
                .Parent.Parent.Parent.Parent;
            for (var i = 0; i < line; i++ )
            {
                var preRow = markRow;
                markRow = (XElement)markRow.NextNode;
                preRow.Remove();
            }
            template.MainDocumentPart.PutXDocument();
            return template;
        }
    }
}
