﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using OpenXmlPowerTools;
using Generate.Tool;
using Generate.Properties;
using MongoDB.Bson;
using System.Xml;
using System.IO.Packaging;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Generate.Model;

namespace Generate.Controller
{
    public class Filler
    {
        public WordprocessingDocument Fill(WordprocessingDocument template, Business business)
        {
            string pattern = @"businesses\.[\w\.\=]+";
            Regex regex = new Regex(pattern);
            XDocument templateXml = template.MainDocumentPart.GetXDocument();
            XElement[] textNodes = templateXml.Descendants(W.t)
                .Where(d => Regex.IsMatch(d.Value, pattern)).ToArray();

            foreach (var textNode in textNodes)
            {
                var text = textNode.Value;
                var matches = regex.Matches(text);
                foreach(var match in matches)
                {
                    var fullname = match + "";
                    var value = this.GetConvertValue(business, fullname);
                    text = text.Replace(fullname, value); 
                }
                textNode.Value = text;
            }
            template.MainDocumentPart.PutXDocument();
            return template;
        }

        public string GetConvertValue(Business business, string fullname)
        {
            var value = business.GetValue(fullname);
            var result = "";

            result = value + "";
            return result;
        }
    }
}
