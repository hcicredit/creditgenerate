﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Generate.Tool
{
    public class Mongo
    {
        //TODO: Move these to Settings
        public const string CONNECT_STRING = @"mongodb://localhost";
        public const string DB_NAME = "test";
        public static MongoDatabase db = new MongoClient(CONNECT_STRING).GetServer().GetDatabase(DB_NAME);
    }
}
