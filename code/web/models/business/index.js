module.exports = (
    function(){
        var mongo = global.mongo;

        var businessSchema = mongo.Schema({
            apply_amount: Number
          , apply_type: String
          , apply_expire: String
          , main_manager: String
          , secondary_manager: String
          , operate_manager: String
          , apply_manager: String
          , apply_date: String
          , company: {
              name: String
            , credit: String
            , legal_represent: String
            , actual_controller: String
            , actual_controller_introduction: String
            , establish_time: String
            , register_cash: Number
            , actual_cash: Number
            , operate_range: String
            , operate_cert_expire: String
            , organ_cert_expire: String
            , is_cert_verify: Boolean
            , industry: String
            , mtype: String
            , size: String
            , client_type: String
            , cash_source: String
            , operate_address: String
            , operate_address_ownership: String
            , operate_address_area: Number
            , operate_address_value: Number
            , is_operate_address_pledge: Boolean
            , operate_address_type: String
            , holder_count: Number
            , staff_count: Number
            , holders: [{
	        name: String
              , cert_number: String
              , amount: Number
              , mtype: String
              , rate: Number
              , main_business: String
              , phone: String
            }]
            , managers: [{
	        name: String
              , position: String
              , age: String
              , education: String
              , title: String
              , expire: Number
              , resumes: [{
                  start: String
                , end: String
                , introduction: String
              }]
            }]
            , staff_structure: {
	        total: Number
              , product: Number
              , technology: Number
              , sale: Number
              , manager: Number
              , master: Number
              , university: Number
              , college: Number
              , vocation: Number
            }
            , relate_companys: [{
	        is_verify: Boolean
              , name: String
              , register_capital: Number
              , amount: Number
              , mtype: String
              , is_other_holding: Boolean
              , holder: String
              , holder_controller_relation: String
              , rate: Number
              , remark: String
              , main_bussiness: String
              , current_asset: Number
              , sale_income: Number
              , current_debt: Number
              , profit: Number
              , total_asset: Number
              , total_debt: Number
              , time: String
              , is_operate: Boolean
              , holders: [{
                  name: String
                , amount: Number
                , mtype: String
                , rate: Number
                , main_bussiness: String
              }]
            }]
            , competitors: [{
	        name: String
              , product: String
              , brand: String
              , local_rate: Number
              , industry_rate: Number
              , advantage: String
              , disadvantage: String
            }]
            , partners: [{
	        mtype: String
              , name: String
              , product: String
              , expire: Number
              , trade_amount: Number
              , payable: Number
              , average_expire: Number
              , total_rate: Number
              , settle_type: String
            }]
            , assured_objects: [{
	        number: String
              , amount: String
              , borrower: String
              , mtype: String
              , over: Number
              , expose: Number
            }]
            , bank_credits: [{
	        name: String
              , amount: Number
              , long_over: Number
              , short_over: Number
              , over: Number
              , deposit: Number
              , expose: Number
              , assure: String
            }]
            , total_bank_credits: [{
	        name: String
              , amount: Number
              , long_over: Number
              , short_over: Number
              , over: Number
              , deposit: Number
              , expose: Number
              , assure: String
            }]
            , asset_debts: [{
	        report_time: String
              , report_type: String
              , cash: Number
              , short_invest: Number
              , receive_bill: Number
              , receivable: Number
              , pre_pay: Number
              , other_receivable: Number
              , inventory: Number
              , await_receivable: Number
              , await_circle_lost: Number
              , expire_asset: Number
              , other_circle_asset: Number
              , attached_company_between: Number
              , total_circle_asset: Number
              , long_invest: Number
              , fixed_origin: Number
              , fixed_lost: Number
              , fixed_real: Number
              , fixed_price: Number
              , fixed_repair: Number
              , current_build: Number
              , await_fixed_lost: Number
              , total_fixed_asset: Number
              , invisible_asset: Number
              , long_wait_pay: Number
              , total_invisible_asset: Number
              , defer_tax: Number
              , total_asset: Number
              , short_debt: Number
              , pay_bill: Number
              , payable: Number
              , pre_receive: Number
              , other_pay: Number
              , pay_wage: Number
              , pay_welfare: Number
              , pay_tax: Number
              , pay_profit: Number
              , other_payable: Number
              , await_payable: Number
              , guess_debt: Number
              , expire_debt: Number
              , other_circle_debt: Number
              , total_circle_debt: Number
              , long_debt: Number
              , pay_bond: Number
              , long_payable: Number
              , other_long_debt: Number
              , total_long_debt: Number
              , total_debt: Number
              , receive_cash: Number
              , addition_cash: Number
              , surplus_cash: Number
              , unallocate_cash: Number
              , total_right: Number
              , total_debt_right: Number
            }]
            , profits: [{
	        report_time: String
              , report_type: String
              , main_income: Number
              , main_cost: Number
              , main_addition: Number
              , main_profit: Number
              , other_profit: Number
              , operate_cost: Number
              , manage_cost: Number
              , finance_cost: Number
              , operate_profit: Number
              , invest_income: Number
              , subsidy_income: Number
              , other_income: Number
              , other_cost: Number
              , total_profit: Number
              , income_tax: Number
              , net_profit: Number
            }]
            , cash_flows: [{
	        report_time: String
              , report_type: String
              , operate_inflow: Number
              , tax_inflow: Number
              , other_operate_inflow: Number
              , total_operate_inflow: Number
              , operate_outflow: Number
              , staff_outflow: Number
              , tax_outflow: Number
              , other_operate_outflow: Number
              , total_operate_outflow: Number
              , total_operate_flow: Number
              , cancel_invest_inflow: Number
              , invest_inflow: Number
              , asset_inflow: Number
              , other_invest_inflow: Number
              , total_invest_inflow: Number
              , asset_outflow: Number
              , invest_outflow: Number
              , bond_outflow: Number
              , other_invest_outflow: Number
              , total_invest_outflow: Number
              , total_invest_flow: Number
              , right_invest_inflow: Number
              , bond_inflow: Number
              , debt_inflow: Number
              , other_financing_inflow: Number
              , total_financing_inflow: Number
              , debt_outflow: Number
              , financing_outflow: Number
              , allocate_profit_outflow: Number
              , interest_outflow: Number
              , lease_outflow: Number
              , cut_cash_outflow: Number
              , other_financing_outflow: Number
              , total_financing_outflow: Number
              , total_financing_flow: Number
              , exchange_include: Number
              , increase_cash: Number
              , debt_capital: Number
              , expire_bond: Number
              , rent_asset: Number
              , net_profit: Number
              , holder_lost: Number
              , asset_lost: Number
              , fixed_asset_lost: Number
              , invisible_asset_amort: Number
              , long_amort: Number
              , cut_amort: Number
              , increase_pre_debt: Number
              , other_asset_lost: Number
              , fixed_asset_scrap: Number
              , finance_outflow: Number
              , invest_lost: Number
              , defer_tax: Number
              , cut_inventory: Number
              , cut_operate_inflow: Number
              , increase_operate_outflow: Number
              , increase_tax: Number
              , other_profit_outflow: Number
              , net_operate_flow: Number
              , cash_end: Number
              , cash_start: Number
              , other_cash_end: Number
              , other_cash_start: Number
              , total_cash_flow: Number
            }]
            , bank_accounts: [{
	        mtype: String
              , name: String
              , account: String
              , over: Number
              , deposit: Number
            }]
            , bills: [{
	        mtype: String
              , amount: Number
              , remark: String
            }]
            , bill_details: [{
	        response: String
	      , amount: Number
	      , create_date: String
	      , expire_date: String
	      , phone: String
	      , relation: String
	      , previous_owner: String
            }]
            , receivables: [{
	        name: String
              , phone: String
              , sale: Number
              , asset: Number
              , reason: String
              , amount: Number
              , expire: String
            }]
            , inventorys: [{
	        name:String
              , mtype: String
              , cost_price: Number
              , market_price: Number
              , purchase_count: Number
              , count: Number
              , address: String
              , is_pledge: Boolean
              , sale_target: String
            }]
            , fixed_assets: [{
	        name: String
              , origin_value: Number
              , book_value: Number
              , cert_number: String
              , owner: String
              , pledge: String
              , address: String
            }]
            , long_invests: [{
	        mtype: String
              , name: String
              , total_invest: Number
              , self_invest: Number
              , expire: Number
              , income: Number
            }]
            , bank_settles: [{
	        number: String
              , name: String
              , borrow_count: Number
              , borrow_amount: Number
              , lend_coumt: Number
              , lend_amount: Number
            }]
            , contract_total: {
                bank_settles_start: String
              , bank_settles_end: String
              , start: String
              , end: String
              , outcome_count: Number
              , outcome: Number
              , income_count: Number
              , income: Number
            }
            , program: {
                assure_type: String
              , assure_person: String
              , expire: String
              , amount: Number
              , status: String
              , expose: Number
              , limit_cycle: Boolean
              , regulate: Boolean
              , total_average: Number
              , settle_average: Number
              , deposit_average: Number
              , mtype: String
              , count: Number
              , start: String
              , end: String
              , deposit: Number
              , risk: Number
              , remark: String
              , pledges: [{
                  mtype: String
                , mtype_name: String
                , name: String
                , owner: String
                , relation: String
                , current_price: Number
                , origin_price: Number
                , assure_cash: Number
                , is_other_bank_pledge: Boolean
                , response: String
                , value: Number
                , number: String
                , expire: String
                , address: String
                , is_verify: Boolean
                , use: String
                , is_transfer: Boolean
                , over_expire: String
                , evaluate_company: String
                , is_listed: Boolean
                , current_value: Number
              }]
              , pawns: [{
	          mtype: String
                , mtype_name: String
                , name: String
                , owner: String
                , relation: String
                , cert_number: String
                , address: String
                , cost: Number
                , value: Number
                , evaluate_company: String
                , lost_rate: Number
                , use_duration: Number
                , over_duration: Number
                , expose_rate: Number
                , is_only: Number
                , rent_rate: Number
                , average_rent_price: Number
                , market_rent_price: Number
                , market_sale_price: Number
                , land_area: Number
                , house_area: Number
                , land_area: Number
                , house_area: Number
              }]
              , varieties: [{
	          mtype: String
                , limit: Number
                , expose: Number
                , price: Number
                , deposit: Number
                , remark: String
                , expire: Number
                , interest_rate: Number
              }]
            }
            , analyze: {
                industry: String
              , history: String
              , honor: String
              , advantage: String
              , market_expand: String
              , industry_chain_position: String
              , industry_position: String
              , program_change: String
              , abnormal_detail: String
              , assured_object_datail: String
              , actual_controller_introduction: String
              , cash_change: String
              , bill_change: String
              , receivable_change: String
              , inventory_change: String
              , fixed_asset_change: String
              , flow_rate: String
              , quick_rate: String
              , debt_rate: String
              , income_rate: String
              , interest_time: String
              , debt_repay_rate: String
              , asset_profit_rate: String
              , sale_profit_rate: String
              , cost_profit_rate: String
              , cash_flow: String
              , finance: String
              , credit_detail: String
              , our_bank_competitive: String
              , long_invest_change: String
            }
            , auto: {
                /*TODO:Move these field*/
                profit_rate: Number
              , inventory_circle_time: Number
              , receivable_circle_time: Number
              , payable_circle_time: Number
              , pre_pay_circle_time: Number
              , pre_receive_circle_time: Number
              , operate_circle_time: Number
              , operate_cash: Number
              , borrower_cash: Number

              , pre_cash_change: Number
              , pre_cash_change_rate: Number
              , cash_change: Number
              , cash_change_rate: Number
              , pre_receivable_change: Number
              , pre_receivable_change_rate: Number
              , receivable_change: Number
              , receivable_change_rate: Number
              , pre_inventory_change: Number
              , pre_inventory_change_rate: Number
              , inventory_change: Number
              , inventory_change_rate: Number
              , pre_fixed_change: Number
              , pre_fixed_change_rate: Number
              , fixed_change: Number
              , fixed_change_rate: Number
              , pre_invest_change: Number
              , pre_invest_change_rate: Number
              , invest_change: Number
              , invest_change_rate: Number

              , first_debt: Number
              , second_debt: Number
              , third_debt: Number
              , pre_debt_change: Number
              , pre_debt_change_rate: Number
              , debt_change: Number
              , debt_change_rate: Number
              , first_income: Number
              , second_income: Number
              , third_income: Number
              , pre_income_change: Number
              , pre_income_change_rate: Number
              , income_change: Number
              , income_change_rate: Number
              , first_interest: Number
              , second_interest: Number
              , third_interest: Number
              , pre_interest_change: Number
              , pre_interest_change_rate: Number
              , interest_change: Number
              , interest_change_rate: Number
              , first_repay: Number
              , second_repay: Number
              , third_repay: Number
              , pre_repay_change: Number
              , pre_repay_change_rate: Number
              , repay_change: Number
              , repay_change_rate: Number
              , first_sale: Number
              , second_sale: Number
              , third_sale: Number
              , pre_sale_change: Number
              , pre_sale_change_rate: Number
              , sale_change: Number
              , sale_change_rate: Number
              , first_profit: Number
              , second_profit: Number
              , third_profit: Number
              , pre_profit_change: Number
              , pre_profit_change_rate: Number
              , profit_change: Number
              , profit_change_rate: Number
              , first_pay: Number
              , second_pay: Number
              , third_pay: Number
              , pre_pay_change: Number
              , pre_pay_change_rate: Number
              , pay_change: Number
              , pay_change_rate: Number
              , first_cost: Number
              , second_cost: Number
              , third_cost: Number
              , pre_cost_change: Number
              , pre_cost_change_rate: Number
              , cost_change: Number
              , cost_change_rate: Number
            }
          }
          , program: {
              assure_type: String
            , expire: String
            , amount: Number
            , status: String
            , expose: Number
            , limit_cycle: Boolean
            , regulate: Boolean
            , total_average: Number
            , settle_average: Number
            , deposit_average: Number
            , mtype: String
            , count: Number
            , start: String
            , end: String
            , deposit: Number
            , risk: Number
            , remark: String
            , pledges: [{
                mtype: String
              , mtype_name: String
              , name: String
              , owner: String
              , relation: String
              , current_price: Number
              , origin_price: Number
              , assure_cash: Number
              , is_other_bank_pledge: Boolean
              , response: String
              , value: Number
              , number: String
              , expire: String
              , address: String
              , is_verify: Boolean
              , use: String
              , is_transfer: Boolean
              , over_expire: String
              , evaluate_company: String
              , is_listed: Boolean
              , current_value: Number
            }]
            , pawns: [{
	        mtype: String
              , mtype_name: String
              , name: String
              , owner: String
              , relation: String
              , cert_number: String
              , address: String
              , cost: Number
              , value: Number
              , evaluate_company: String
              , lost_rate: Number
              , use_duration: Number
              , over_duration: Number
              , expose_rate: Number
              , is_only: Number
              , rent_rate: Number
              , average_rent_price: Number
              , market_rent_price: Number
              , market_sale_price: Number
              , land_area: Number
              , house_area: Number
              , land_area: Number
              , house_area: Number
            }]
            , varieties: [{
	        mtype: String
              , limit: Number
              , expose: Number
              , price: Number
              , deposit: Number
              , remark: String
              , expire: Number
              , interest_rate: Number
            }]
            , analyze: {
                sale_increase_rate: Number
              , flow_loan: Number
              , other_operate_cash: Number
              , other_flow_load: Number

              , rent: String
              , rent_repay: String
              , rent_price: String
              , rent_operate: String
              , contract: String
              , contract_repay: String
              , contract_price: String
              , contract_operate: String
              , bill: String
              , bond: String
              , lading: String
              , lading_operate: String
              , brand: String
              , stock: String
              , other_pledge: String
              , assure: String
              , other_assure: String
              , use: String
              , reasonable: String
              , operate_detail: String
              , income: String
              , bill_income: String
              , interest_income: String
              , deposit_income: String
              , settle_income: String
              , formality_income: String
              , other_income: String
              , summary: String
              , risk_prevent: String
              , other_risk: String
            }
          }
          , assure_persons: [{
              mtype: String
            , name: String
            , is_holder: Boolean
            , age: Number
            , education: String
            , title: String
            , abnormal_detail: String
            , ability: String
            , assets: [{
	        mtype: String
              , owner: String
              , owner_cert: String
              , address: String
              , is_pledge: Boolean
              , market_price: Number
            }]
            , assured_objects: [{
	        number: String
              , amount: String
              , borrower: String
              , mtype: String
              , over: Number
              , expose: Number
            }]
            , bank_settles: [{
	        number: String
              , name: String
              , borrow_count: Number
              , bowwow_amount: Number
              , lend_count:Number
              , lend_amount: Number
            }]
          }]
        });

        var Business = mongo.model('Business', businessSchema);

        return Business;
    }
).call(this);
