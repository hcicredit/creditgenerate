module.exports = (function(){
    var SQL = {
	add: 'INSERT INTO $table SET $modifier',
	del: 'DELETE FROM $table $selector',
	get: 'SELECT * FROM $table $selector',
	update: 'UPDATE $table SET $modifier $selector',
	convert: function(cmd){
	    var act = cmd.act,
		selector = cmd.selector,
		modifier = cmd.modifier;
	    var table = selector.table,
		sql,
		selectorSql = '',
		modifierSql = '',
		originSql = this[act];
	    delete selector.table;
	    sql = originSql.replace('$table', table);

	    for(var k in selector){
		selectorSql += k + '=' + selector[k] + ' AND ';
	    };
	    if(selectorSql.length !== 0){
		selectorSql = 'WHERE ' + selectorSql;
		selectorSql = selectorSql.slice(0, -5);
	    };
	    for(k in modifier){
		modifierSql += k + '=\'' + modifier[k] + '\', ';
	    };
	    if(modifierSql.length !== 0){
		modifierSql = modifierSql.slice(0, -2);
	    };

	    sql = sql.replace('$selector', selectorSql);
	    sql = sql.replace('$modifier', modifierSql);

	    console.log(sql);
	    return sql;
	}
    };

    var mysql = require('mysql'),
	conn;

    var query = function(cmd, fn){
    conn = mysql.createConnection({
	   	host: 'localhost',
	   	user: 'root',
	   	database: 'credit_generate'
	});
	conn.connect();
	conn.query(SQL.convert(cmd), function(err, result){
	    if(err) throw err;
	    fn(result);
	});
	conn.end();
    };

    return {
	query: query
    };
}).call(this);
