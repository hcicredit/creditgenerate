module.exports = (
    function(){
        var mongo = global.mongo
          , crypto = require('crypto');

        var userSchema = mongo.Schema({
            name: {type: String, unique: true}
          , hashed_password: String
          , salt: String
          , businesses : [{
              name: String
            , id: String
            , date: Date
          }]
        });

        userSchema
        .virtual('password')
        .set(function(password) {
            this._password = password;
            this.salt = this.makeSalt();
            this.hashed_password = this.encryptPassword(password);
        })
        .get(function() { return this._password; });

        userSchema.method('auth', function(plainText) {
            return this.encryptPassword(plainText) === this.hashed_password;
        });

        userSchema.method('makeSalt', function() {
            return Math.round((new Date().valueOf() * Math.random())) + '';
        });

        userSchema.method('encryptPassword', function(password) {
            return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
        });

        var User = mongo.model('User', userSchema);

        return User;
    }
).call(this);
