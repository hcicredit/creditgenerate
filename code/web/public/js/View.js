var View = window.View = (
    function(){
        var App = Backbone.View.extend({
            el: $('body')
          , events: {
              'click .ex-save': 'save'
            , 'click .ex-generate': 'generate'
          }
          , initialize: function(){
                this.load();
            }
          , load: function(){
                var flag = 0;
                var that = this;
                var callback = function(){
                    flag++;
                    if(flag === $('.ex-load').length){
                        console.log('Finish loading...');
                    };
                };

                $('.ex-load').each(function(){
                    var id = $(this).attr('id')
                      , dir = id.split('-')[1]
                      , name = id.split(dir + '-')[1];
                    $(this).load(dir + '/' + name + '.html', callback);
                });
            }
          , save: function(){
                $('.ex-business').submit();
            }
          , generate: function(){
                Pub.businessView.generate();
            }
          , toSection: function(name){
                //TODO: Add more strictly permission check.
                $('.ex-section').hide();
                $('.ex-' + name).show();
                $('.ex-nav').hide();
                $('.ex-nav-' + name).show();
            }
          , loading: function(){
                $('.ex-loading-modal').modal('show');
            }
          , complete: function(){
                $('.ex-loading-modal').modal('hide');
            }
          , alert: function(msg){
                $('.ex-alert-modal h4').text(msg);
                $('.ex-alert-modal').show();
                setTimeout(function(){
                    $('.ex-alert-modal').fadeOut('slow');
                }, 1000);
            }
        });

        var User = Backbone.View.extend({
            el: $('.ex-user')
          , model: Model.User
          , events: {
              'submit #ex-sign-in': 'signIn'
            , 'submit #ex-sign-up': 'signUp'
            , 'submit #ex-sign-out': 'signOut'
          }
          , toSign: function(action){
                var $alertbox = $('.ex-user .ex-alert');
                $alertbox.hide();

                $('.ex-sign', this.el).hide();
                $('#ex-sign-' + action, this.el).show();
            }
          , success: function(){
                var $alertbox = $('.ex-user .ex-alert');
                $alertbox.hide();
                var issueView = Pub.issueView = new Issue();
                Pub.appRouter.navigate('issue', true);
            }
          , error: function(msg){
                var $alertbox = $('.ex-user .ex-alert');
                $alertbox.show();
                $alertbox.text(msg);
            }
          , signIn: function(e){
                e.preventDefault();
                var $alertbox = $('.ex-user .ex-alert');
                $alertbox.hide();

                var data = this.convertFormData($(e.currentTarget).serializeArray());
                var user = Pub.user = new Model.User(data);
                user.signIn(this.success, this.error);
            }
          , signUp: function(e){
                e.preventDefault();
                var $alertbox = $('.ex-user .ex-alert');
                $alertbox.hide();

                var data = this.convertFormData($(e.currentTarget).serializeArray());
                if(data.confirm_password === data.password){
                    var user = Pub.user = new Model.User(data);
                    user.signUp(this.success, this.error);
                }else{
                    var msg = 'Password is no equal to Confirm Password.';
                    this.error(msg);
                };
            }
          , signOut: function(e){
                var $alertbox = $('.ex-user .ex-alert');
                $alertbox.hide();
                Pub.user.signOut();
            }
          , convertFormData: function(formData){
                var data = {};
                for(var i = 0, l = formData.length; i < l; i++){
                    var formDatum = formData[i];
                    data[formDatum.name] = formDatum.value;
                };
                return data;
            }
        });

        var Issue = Backbone.View.extend({
            el: $('.ex-issue')
          , events: {
              'click .ex-add-business': 'addBusiness'
            , 'click .ex-show-business': 'showBusiness'
            , 'click .ex-del-business': 'delBusiness'
          }
          , initialize: function(){
                this.render();
            }
          , render: function(){
                var businesses = Pub.user.get('businesses');
                var template = _.template($('#template-user-issue').html());
                var html = template({businesses: businesses});
                this.$el.html(html);
            }
          , addBusiness: function(e){
                var modal = e.currentTarget.parentElement.parentElement;

                var user = Pub.user
                  , businesses = user.get('businesses');

                var name = $('.ex-add-business-name', modal).val()
                  , date = new Date();

                Pub.businesses.create({},{
                    success: function(model){
                        console.log('Create business successfully.');
                        businesses.push({name: name, id: model.id, date: date});
                        user.save();

                        var businessView = Pub.businessView = new Business({id: model.id, model: model});
                        var snippetView = Pub.snippetView = new Snippet({model: model});
                        snippetView.autoSetFinance();

                        Pub.appRouter.navigate('business', true);
                        $('#ex-add-business-modal').modal('hide');
                    }
                });
            }
          , showBusiness: function(e){
                Pub.appView.loading();

                var d = e.currentTarget
                  , id = $(d).attr('data-id')
                  , business = Pub.businesses.get(id);

                var businessView = Pub.businessView = new Business({id: id, model: business});
                var snippetView = Pub.snippetView = new Snippet({model: business});

                var delay = function(){
                    businessView.restore();
                    Pub.appRouter.navigate('business', true);

                    Pub.appView.complete();
                };
                setTimeout(delay, 10);
            }
          , delBusiness: function(e){
                var d = e.currentTarget
                  , tr = d.parentElement.parentElement;
                var id = $(d).attr('data-id')
                  , name = $(d).attr('data-name');

                var $modal = $('#ex-del-confirm-business-modal');
                $('.ex-del-confirm-business', $modal).click(function(){
                    var businesses = Pub.businesses;
                    $(tr).remove();
                    businesses.remove(businesses.get(id));

                    businesses = Pub.user.get('businesses');
                    var business = _(businesses).where({id: id})[0];
                    businesses = _(businesses).without(business);
                    Pub.user.set({businesses: businesses});
                    Pub.user.save();
                    $modal.modal('hide');
                });
            }
        });

        var Business = Backbone.View.extend({
            el: $('.ex-business')
          , catalogEl: $('.ex-business-catalog')
          , partsEl: $('.ex-business-parts')
          , parts: ['basic', 'asset', 'profit', 'cashflow', 'program', 'company-intro', 'company-holder', 'company-relate', 'program-purpose', 'program-analyze', 'program-reasonable', 'company-position', 'company-credit', 'company-contract', 'analyze-operate', 'analyze-debt', 'analyze-long-debt', 'analyze-profit', 'analyze-cash', 'analyze-finance', 'assure-ensure', 'assure-pledge', 'assure-pawn', 'assure-credit', 'assure-other', 'cooperate', 'risk', 'operate', 'income', 'conclusion']
          , initialize: function(){
                var id = this.id;
                Pub.business = this.model;
                this.renderCatalog();
                this.renderParts(id);

                var autoView = Pub.autoView = new Auto();
            }
          , events: {
              'click .ex-datepicker': 'toggleDate'
            , 'submit': 'submit'
          }
          , restore: function(){
                var snippetView = Pub.snippetView;
                snippetView.autoExistSnippet();
                snippetView.autoAddSnippet();
                snippetView.autoAddRow();
                snippetView.autoAddCol();
                snippetView.autoSetFinance();

                var kvs = this.deconvert(this.model.toJSON());
                for(var i = 0, l = kvs.length; i < l; i++){
                    var kv = kvs[i]
                      , name = kv.name
                      , value = kv.value;
                    $('[name="' + name + '"]').val(value + '');
                    $('[name="' + name + '"].ex-auto-source').change();
                };
            }
          , toPart: function(name){
                $('.ex-part', this.partsEl).hide();
                $('#ex-part-' + name, this.partsEl).show();
            }
          , renderParts: function(id){
                var business = Pub.businesses.get(id)
                  , parts = this.parts;
                var html = ''
                  , template;
                for(var i = 0, l = parts.length; i < l; i++){
                    template = _.template($('#template-part-' + parts[i]).html());
                    html += template(business && business.toJSON());
                };
                this.partsEl.html(html);
            }
          , renderCatalog: function(){
                var template = _.template($('#template-business-catalog').html())
                  , html = template();
                this.catalogEl.html(html);
            }
          , render: function(id){
                this.renderCatalog();
                this.renderParts(id);
                this.getSnippets();
            }
          , toggleDate: function(e){
                var d = e.currentTarget;
                $(d).datepicker('show');
            }
          , submit: function(e){
                e.preventDefault();

                var d = e.currentTarget;
                var kvs = $(d).serializeArray();
                var that = this;
                var business = this.convert(this.filter(kvs));
                business = Pub.business = this.model.save(
                    business
                  , {
                      success: function(model){
                          Pub.appView.alert('Save business successfully.');
                          Pub.business = model;
                      }
                  });
            }
          , generate: function(){
                Pub.appView.loading();
                this.model.generate();
          }
          , finish: function(){
                Pub.appView.complete();
                this.model.download();
          }
          , filter: function(kvs){
                kvs = kvs.map(function(e){
                          if(e.value === 'false' || e.value === 'true'){
                              e.value = JSON.parse(e.value);
                          };
                          return e;
                      }).filter(function(e){
                          if(e === undefined)
                            return false;
                          else
                            return true;
                      });
                return kvs;
            }
          , convert: function(kvs){
                var business = {};

                for(var i = 0, l = kvs.length; i < l; i++){
                    var kv = kvs[i]
                      , keys = kv.name.split('.')
                      , value = kv.value
                      , key = keys[0]
                      , current = business;
                    for(var j = 0, k = keys.length; j < k - 1; j++){
                        key = keys[j];
                        if(current[key] === undefined){
                            current[key] = {};
                        };
                        current = current[key];
                    };
                    current[keys[j]] = value;
                };

                this._toArr(business);

                return business;
            }
          , deconvert: function(business){
                //From Object to value.
                var kvs = [];
                var getKvs = function(preK, preV){
                  if(_.isObject(preV)){
                    for(var k in preV){
                      var v = preV[k];
                      getKvs(preK + '.' + k, v);
                    };
                  }else{
                    kvs.push({name: preK, value: preV});
                  };
                };

                for(var k in business){
                    getKvs(k, business[k]);
                };

                return kvs;
            }
          , _toArr: function(e){
                for(var k in e){
                  if(!_.isString(e)){
                    var tempArr = [];
                    for(var j in e[k]){
                      if(!_.isString(e[k])){
                        if(isFinite(j - 0)){
                            tempArr.push(e[k][j]);
                        };
                      };
                    };
                    if(tempArr.length > 0){
                        e[k] = tempArr;
                    };
                    this._toArr(e[k]);
                  };
                };
            }
          , test: function(){
	        $('input').each(function(){
                    //var name = ($(this).attr('field-name') || $(this).attr('name'));
                    //$(this).attr('placeholder', name);
                    var type = $(this).attr('type');
                    if($(this).attr('class') && $(this).attr('class').match('ex-datepicker')){
                        this.value = "2013-1-28";
                    }else if(type.match('text')){
                        if(!this.value){
                            this.value = 'text';
                        };
                    }else if(type.match('number')){
                        this.value = 4;
                    };
                });
                $('textarea').each(function(){
                    this.value = 'textarea';
                });
            }
        });

        var Snippet = Backbone.View.extend({
            el: $('.ex-business')
          , inc: (function(){
                var inc = 0;
                return function(){
                    return inc++;
                };
            })()
          , events: {
              'change .ex-snippet-exist': 'existSnippet'
            , 'click .ex-snippet-add-row': 'addRow'
            , 'click .ex-snippet-del-row': 'delRow'
            , 'click .ex-snippet-add-col': 'addCol'
            , 'click .ex-snippet-del-col': 'delCol'
            , 'click .ex-snippet-add': 'addSnippet'
            , 'click .ex-snippet-del': 'delSnippet'
            , 'change .ex-snippet-set-finance': 'setFinance'
            , 'change .ex-snippet-set-pawns': 'setPawns'
            , 'change .ex-snippet-set-pledges': 'setPledges'
          }
          , initialize: function(){
            }
          , setPledges: function(e){
                var $table = $(e.currentTarget)
                  , $div = $table.parent();
                var $rent = $('.ex-snippet-pledge-rent', $div)
                  , $contract = $('.ex-snippet-pledge-contract', $div)
                  , $bill = $('.ex-snippet-pledge-bill', $div)
                  , $bond = $('.ex-snippet-pledge-bond', $div)
                  , $lading = $('.ex-snippet-pledge-lading', $div)
                  , $brand = $('.ex-snippet-pledge-brand', $div)
                  , $stock = $('.ex-snippet-pledge-stock', $div)
                  , $other = $('.ex-snippet-pledge-other', $div);
                var that = this;

                $('div.ex-snippet-pledge table', $div).each(function(){
                    var $tr = $('tr:gt(2)', this);
                    $tr.remove();
                });
                $('div.ex-snippet-pledge', $div).hide();

                $('select.ex-snippet-pledge-type[name]', $table).each(function(){
                    var $target;
                    if(this.value === '物业租赁应收账款'){
                        $target = $rent;
                    }else if(this.value === '供应链应收账款'){
                        $target = $contract;
                    }else if(this.value === '银行承兑汇票'){
                        $target = $bill;
                    }else if(this.value === '债券' || this.value === '存单'){
                        $target = $bond;
                    }else if(this.value === '仓单' || this.value === '提单'){
                        $target = $lading;
                    }else if(this.value === '商标权' || this.value === '专利权'){
                        $target = $brand;
                    }else if(this.value === '股权'){
                        $target = $stock;
                    }else if(this.value === '其他'){
                        $target = $other;
                    };

                    $target.show();
                    var $tr = $('tr.ex-snippet', $target).clone();
                    var cla = $tr.attr('class');
                    if(cla){
                        $tr.attr('class', cla.replace('ex-snippet', ''));
                    };
                    $tr.show();
                    var name = $(this).attr('name');
                    var number = name.match(/\d+/)[0] - 0;
                    that.transArrName($tr, number);
                    $('tbody', $target).append($tr);
                });
            }
          , setPawns: function(e){
                var $table = $(e.currentTarget)
                  , $div = $table.parent();
                var $building = $('.ex-snippet-pawn-building', $div);
                var that = this;

                $('div.ex-snippet-pawn table', $div).each(function(){
                    var $tr = $('tr:gt(1)', this);
                    $tr.remove();
                });
                $('div.ex-snippet-pawn', $div).hide();

                $('select.ex-snippet-pawn-type[name]', $table).each(function(){
                    var $target;
                    if(this.value === '房产' || this.value === '土地' || this.value === '在建工程'){
                        $target = $building;
                        $target.show();
                        var $tr = $('tr.ex-snippet', $target).clone();
                        $tr.attr('class', $tr.attr('class').replace('ex-snippet', ''));
                        $tr.show();
                        var name = $(this).attr('name');
                        var number = name.match(/\d+/)[0] - 0;
                        that.transArrName($tr, number);
                        $('tbody', $target).append($tr);
                    };
                });
            }
          , autoExistSnippet: function(){
                var business = this.model;
                $('.ex-snippet-exist').each(function(){
                    var $snippet = $(this.parentElement).next();
                    var name = $(this).data('target');

                    if(name !== '' && name !== undefined){
                        var isExist = business.isKeyExist(name);
                        if(isExist){
                            $(this).val('true');
                            $(this).trigger('change');
                        };
                    };
                });
            }
          , existSnippet: function(e){
                var d = e.currentTarget
                  , v = JSON.parse((d.checked !== undefined) || d.value);
                var $snippet = $(d.parentElement).next();
                if(v){
                    var $target = $snippet.clone().show();
                    $snippet.after($target);
                }else{
                    var $target = $snippet.next();
                    $target.remove();
                };
            }
          , autoAddSnippet: function(){
                var business = this.model;
                $('.ex-snippet-add').each(function(){
                    var $div = $(this.parentElement.parentElement);
                    var field = $('[field-name]', $div).attr('field-name');

                    if(field !== undefined){
                        var arrPrefix = field.split('.arr')[0];
                        var l = business.getArrLength(arrPrefix);
                        for(var i = 0; i < l; i++){
                            $(this).trigger('click', i);
                        };
                    };
                });
            }
          , addSnippet: function(e, i){
                var $div = $(e.currentTarget.parentElement.parentElement)
                  , $snippet = $('div.ex-snippet', $div).clone();
                $snippet.attr('class', $snippet.attr('class').replace('ex-snippet', ''));
                $snippet.show();
                var number = i;
                if(number === undefined){
                    number = this.inc();
                };
                this.transArrName($snippet, number);
                $div.append($snippet);
                $('.ex-auto-source', $snippet).change();
            }
          , delSnippet: function(e){
                var $snippet = $(e.currentTarget);
                while($snippet.prop('tagName') !== 'DIV'){
                    $snippet = $snippet.parent();
                };
                $('.ex-auto-source', $snippet).val('').change();
                $snippet.remove();
            }
          , autoAddRow: function(){
                var business = this.model;
                $('.ex-snippet-add-row').each(function(){
                    var tbody = this.parentElement.parentElement.parentElement;
                    var field = $('[field-name]', tbody).attr('field-name');

                    if(field !== undefined){
                        var arrPrefix = field.split('.arr')[0];
                        var l = business.getArrLength(arrPrefix);
                        for(var i = 0; i < l; i++){
                            $(this).trigger('click', i);
                        };
                    };
                });
            }
          , addRow: function(e, i){
                var tbody = e.currentTarget.parentElement.parentElement.parentElement
                  , $tr = $('tr.ex-snippet', tbody).clone();
                $tr.attr('class', $tr.attr('class').replace('ex-snippet', ''));
                $tr.show();
                var number = i;
                if(number === undefined){
                    number = this.inc();
                };
                this.transArrName($tr, number);
                $(tbody).append($tr);
                $('.ex-auto-source', $tr).change();
            }
          , delRow: function(e){
                var tbody = e.currentTarget.parentElement.parentElement.parentElement;
                var l = $('tr.ex-snippet', tbody).length;
                var $tr = $(e.currentTarget.parentElement.parentElement)
                  , $preTr = $tr;
                for(var i = 0; i < l ; i++){
                    $tr = $tr.next();
                    $('.ex-auto-source', $preTr).val('').change();
                    $preTr.remove();
                    $preTr = $tr;
                };
            }
          , autoAddCol: function(){
                //TODO:
                var business = this.model;
                $('.ex-snippet-add-col').each(function(){
                    var tbody = this.parentElement.parentElement.parentElement;
                    var field = $('[field-name]', tbody).attr('field-name');

                    if(field !== undefined){
                        var arrPrefix = field.split('.arr')[0];
                        var l = business.getArrLength(arrPrefix);
                        for(var i = 0; i < l; i++){
                            $(this).trigger('click', i);
                        };
                    };
                });
            }
          , addCol: function(e, i){
                var tbody = e.currentTarget.parentElement.parentElement.parentElement;
                var number = i;
                if(number === undefined){
                    number = this.inc();
                };
                var that = this;
                $('.ex-snippet', tbody).each(function(){
                    var tr = this.parentElement
                      , $td = $(this).clone();
                    $td.attr('class', $td.attr('class').replace('ex-snippet', ''));
                    $td.show();
                    that.transArrName($td, number);
                    $(tr).append($td);
                    $('.ex-auto-source', $td).change();
                });
            }
          , delCol: function(e){
                var tbody = e.currentTarget.parentElement.parentElement.parentElement
                  , index = $(e.currentTarget.parentElement).index();

                $('tr', tbody).each(function(){
                    var $td = $(this).children().eq(index)
                    $('.ex-auto-source', $td).val('').change();
                    $td.remove();
                });
            }
          , setFinance: function(e){
                var number = e.currentTarget.value - 0;
                var $table = $(e.currentTarget.parentElement).next();
                var $tds = $('tr:first-child td', $table);
                if($tds.length - 2 < number){
                    for(var i = $tds.length - 2; i < number; i++){
                        $('.ex-snippet-add-col', $tds).trigger('click', i);
                    };
                }else if($tds.length - 2 > number){
                    for(var i = $tds.length - 2; i > number; i--){
                        var $td = $('tr:first-child td:last-child', $table);
                        $('.ex-snippet-del-col', $td).trigger('click');
                    };
                };

                $('tr:eq(1) td input', $table).each(function(){
                    $(this).datepicker()
                    .on('changeDate', function(e){
                        $(this).change();
                    })
                    .on('hide', function(e){
                        $(this).change();
                    });
                });


                var $timeTr = $('tr:eq(1)', $table)
                  , $typeTr = $('tr:eq(2)', $table);
                $('td:lt(4) input[name]', $timeTr).attr('disabled', 'disabled');
                $('td:lt(5) select[name]', $typeTr).val('年报').attr('disabled', 'disabled');
            }
          , autoSetFinance: function(){
                var business = this.model;
                $('.ex-snippet-set-finance').each(function(){
                    var table = $(this.parentElement).next();
                    var field = $('[field-name]', table).attr('field-name');

                    if(field !== undefined){
                        var arrPrefix = field.split('.arr')[0];
                        var l = business.getArrLength(arrPrefix);
                        this.value = l >= 4? l + '': 4 + '';
                        $(this).trigger('change');
                    };
                });
            }
          , transArrName: function(target, number){
                var $target = $(target);
                //console.log(target[0]);
                var $inputs = $('[field-name]', $target);
                //console.log($inputs[0]);
                $inputs.each(function(){
                    var fieldName = $(this).attr('field-name');
                    fieldName = fieldName.replace('arr', number);
                    if(fieldName.match('arr')){
                        $(this).attr('field-name', fieldName);
                    }else{
                        $(this).attr('field-name', '');
                        $(this).attr('name', fieldName);
                    };
                });

                var $selects = $('[field-data-target]', $target);
                $selects.each(function(){
                    var fieldTarget = $(this).attr('field-data-target');
                    fieldTarget = fieldTarget.replace('arr', number);
                    if(fieldTarget.match('arr')){
                        $(this).attr('field-data-target', fieldTarget);
                    }else{
                        $(this).attr('field-data-target', '');
                        $(this).attr('data-target', fieldTarget);
                    };
                });

                //TODO: test this code snippet.
                var $targets = $('[data-equation]', $target);
                var regex = /[\+\-\/\*\(\)\ \!\=\>\<\|\&]/;
                $targets.each(function(){
                    var equation = $(this).data('equation');
                    var flag = false;

                    equation
                    .split(regex)
                    .filter(function(e){
                        return e.length > 0? true: false;
                    })
                    .map(function(e){
                        if(e.match(/arr/g).length > 1){
                            var regex = new RegExp(e, 'g');
                            equation.replace(regex, e.replace('arr', number));
                        };
                    });

                    $(this).attr('data-equation', equation);
                });
            }
        });

        var Auto = Backbone.View.extend({
                       el: $('.ex-business')
                     , initialize: function(){
                       }
                     , events: {
                         'change .ex-auto-source-compute': 'compute'
                       , 'change .ex-auto-sum': 'sum'
                       , 'change .ex-auto-source-date': 'date'
                         //, 'change .ex-auto-exist-value': 'existValue'
                     }
                     , date: function(e){
                           var index = $(e.currentTarget.parentElement).index();
                           if(index !== 5) return;

                           var $tbody = $(e.currentTarget.parentElement.parentElement.parentElement);
                           var $timeTr = $('tr:eq(1)', $tbody);
                           var time = $('td:eq(4) input', $timeTr).val();
                           var regex = /(\d+)-(\d+)/;
                           if(regex.exec(time)){
                               var year = regex.exec(time)[1];
                               var month = regex.exec(time)[2];
                               if(month - 0 === 1){
                                   --year;
                               };

                               year = year - 3;
                               $('td:lt(4) input[name]', $timeTr).each(function(){
                                   $(this).val(year++);
                               });
                           };
                       }
                     , sum: function(e){
                           var table = e.currentTarget
                             , input = e.target;

                           //Find Target Input.
                           var td;
                           var parent = input;
                           while((parent = parent.parentElement) && parent.nodeName !== 'TD');
                           td = parent;
                           var index = $(td).index();
                           var result = $('.ex-auto-result-sum td:eq(' + index + ') input', table)[0];
                           parent = result;
                           if (parent === undefined) return;

                           var tr;
                           while((parent = parent.parentElement) && parent.nodeName !== 'TR');
                           tr = parent;

                           var value = 0;
                           var $next = $(tr).next();
                           while($next.length !== 0){
                               value += $('td:eq(' + index + ') [name]', $next).val() - 0;
                               $next = $next.next();
                           };
                           value = JSON.parse(value.toFixed(4));
                           result.value = value;
                       }
            /*, existValue: function(e){
              var d = e.currentTarget;

              }*/
                     , getTargets: function(source){
                           var name = source.name;
                           var regex = /[\+\-\/\*\(\)\ \!\=\>\<\|\&]/;
                           //TODO: Cache this.
                           var $targets = $('[data-equation]').filter(function(){
                                              var equation = $(this).data('equation');
                                              var flag = false;

                                              equation
                                              .split(regex)
                                              .filter(function(e){
                                                  return e.length > 0? true: false;
                                              })
                                              .map(function(e){
                                                  if(e.match('arr')){
                                                      var regex = new RegExp(e.replace('arr', '\\d+'));
                                                      if(regex.exec(name)){
                                                          flag = true;
                                                      };
                                                  }else{
                                                      if(e === name){
                                                          flag = true;
                                                      };
                                                  };
                                              });

                                              return flag;
                                          });
                           return $targets;
                       }
                     , compute: function(e){
                           var d = e.target;
                           var $targets = this.getTargets(d);
                           var regex = /[\+\-\/\*\(\)\ \!\=\>\<\|\&]/;

                           $targets.each(function(){
                               var equation = $(this).data('equation');

                               equation
                               .split(regex)
                               .filter(function(e){
                                   return e.length > 0? true: false;
                               })
                               .map(function(e){
                                   var value;
                                   if(e.match('arr')){
                                       value = 0;
                                       var prefix = e.split('arr')[0]
                                         , suffix = e.split('arr')[1];
                                       var $inputs = $('[name^="' + prefix + '"]');
                                       $inputs = $inputs.filter(function(){
                                                     var regex = new RegExp(e.replace('arr', '\\d+'));
                                                     if(regex.exec(this.name)){
                                                         return true;
                                                     }else{
                                                         return false;
                                                     };
                                                 });

                                       $inputs.each(function(){
                                           var currentValue = $(this).val() - 0;
                                           if(isNaN(currentValue)){
                                               value = value || '';
                                               value += ' ' + $(this).val();
                                           }else{
                                               value = value || 0;
                                               value += currentValue;
                                           };
                                       });
                                   }else{
                                       var currentValue = $('[name="' + e + '"]').val() - 0;
                                       if(isNaN(currentValue)){
                                           value = $('[name="' + e + '"]').val();
                                       }else{
                                           value = currentValue;
                                       };
                                   };

                                   if(_.isNumber(value)){
                                       value = value.toFixed(4);
                                   }else if(_.isString(value)){
                                       value = '\'' + value + '\'';
                                       equation = equation.replace(/\//g, '.search');
                                   };
                                   if(value !== undefined){
                                       var regex = new RegExp(e, 'g');
                                       equation = equation.replace(regex, value);
                                   };
                               });

                               var value;
                               try{
                                   value = eval(equation);
                               }catch(err){
                                   console.log(err);
                               };

                               if(_.isBoolean(value)){
                                   if($(this).val() !== value + ''){
                                       $(this).val(value + '');
                                       $(this).trigger('change');
                                   };
                               }else if(value !== undefined){
                                   $(this).val(value);
                               };
                           });
                       }
                   });

        return {
            App: App
          , User: User
          , Business: Business
          , Snippet: Snippet
          , init: function(){
                var appView = Pub.appView = new App();
                var userView = Pub.userView = new User();
            }
        };
    }
).call(this);