var Router = window.Router = (
    function(){
        var App = Backbone.Router.extend({
            routes: {
                ':name': 'toSection'
              , 'business/part/:name': 'toPart'
              , 'user/sign/:action': 'toSign'
            }
          , initialize: function(){
                Backbone.history.start();
                this.checkUser();
                /*Backbone.sync = function(){
                    console.log(arguments);
                };*/
            }
          , toSection: function(name){
                if(name != 'user'){
                    this.checkUser() && Pub.appView.toSection(name);
                }else{
                    Pub.appView.toSection(name);
                };
            }
          , toPart: function(name){
                this.checkUser() && Pub.businessView.toPart(name);
            }
          , toSign: function(action){
                Pub.userView.toSign(action);
            }
          , checkUser: function(){
                var result = false;
                if(!Pub.user){
                    this.navigate('user', true);
                }else{
                    result = true;
                };
                return result;
            }
        });

        return {
            App: App
          , init: function(){
                var appRouter = Pub.appRouter = new App();
            }
        };
    }
).call(this);