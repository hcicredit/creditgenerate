var socket = window.socket = (
    function(){
        var socket = io.connect('http://localhost:3000');

        socket.on('fromforward', function(data){
          if(data.act === 'result'){
            if(data.id === Pub.business.get('_id')){
              Pub.businessView.finish();
            };
          };
        });

        return socket;
    }
).call(this);