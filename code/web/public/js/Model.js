var Model = window.Model = (
    function(){
        var User = Backbone.Model.extend({
            initialize: function(){
            }
          , idAttribute: '_id'
          , url: '/api/model/user'
          , signIn: function(successFn, errorFn){
                var data = this.toJSON();
                var that = this;
                var url = '/api/model/user/sign_in';
                $.ajax({
                    type: 'post'
                  , url: url
                  , data: data
                }).success(function(data){
                    if(data){
                        that.set(data);
                        successFn && successFn();
                    };
                }).fail(function(){
                    var msg = 'Check User Name or Password.';
                    errorFn && errorFn(msg);
                });
            }
          , signUp: function(successFn, errorFn){
                var data = this.toJSON();
                var that = this;
                var url = '/api/model/user/sign_up';
                $.ajax({
                    type: 'post'
                  , url: url
                  , data: data
                }).success(function(data){
                    if(data){
                        that.set(data);
                        successFn && successFn();
                    };
                }).fail(function(){
                    var msg = 'Exist Name.';
                    errorFn && errorFn(msg);
                });
            }
          , signOut: function(){
            }
          /*, save: function(fn){
                var data = this.toJSON();
                var that = this;
                var url = '/api/model/user/' + this.get('_id');
                $.ajax({
                    type: 'put'
                  , url: url
                  , data: data
                }).success(function(data){
                    if(data){
                        that.set(data);
                        fn && fn();
                    };
                });
            }*/
        });

        var Business = Backbone.Model.extend({
            urlRoot: '/api/model/business'
          , idAttribute: '_id'
          , initialize: function(){
                this.bind('remove', function() {
                    this.destroy();
                });
            }
          , isKeyExist: function(name){
                var v = this.getValue(name);
                var result = true;
                if(v === undefined || v === null || v === ''){
                    result = false;
                };
                if(_.isArray(v) && v.length === 0){
                    result = false;
                };
                return result;
            }
          , getArrLength: function(arrPrefix){
                var value = this.getValue(arrPrefix);
                var l = 0;
                l = (value && value.length) || 0;
                return l;
            }
          , getValue: function(name){
                var keys = name.split('.');
                var v = this.get(keys[0]);
                for(var i = 1, l = keys.length; i < l &&  v !== undefined; i++){
                    var k = keys[i];
                    v = v[k];
                };
                return v;
            }
          , generate: function(){
              var data = {act: 'generate', id: this.id};
              socket.emit('forward', data);
            }
          , download: function(){
              console.log('start download');
              window.location.assign('/doc/' + this.id + '.docx');
          }
        });

        return {
            Business: Business
          , User: User
        };
    }
).call(this);