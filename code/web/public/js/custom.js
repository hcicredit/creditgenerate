var input_width_adaption = function() {
	$('.ex-input-width input').css({
		'width' : '80px',
	});
	$('.ex-input-width tr input:first').css({
		'width' : '100px',
	});

	/*
	 $('.ex-input-width input').css({
	 'width' : $('.ex-input-width tr:nth-child(2) th:nth-child(i)'),
	 });
	 */
	$('.ex-input-width-5col input').css({
		'width' : $('.ex-input-width-5col input').parents('td').length,
	});
	
	$('.ex-input-width-12col input').css({
		'width' : $('.ex-input-width-12col input').parents('td').length,
	});
};

input_width_adaption.call(window);
