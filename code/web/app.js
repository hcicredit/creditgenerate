var express = require('express'),
    app = global.app = express(),
    http = require('http'),
    conf = require('./conf'),
    server = http.createServer(app),
    io = global.io = require('socket.io').listen(server),
    mongo = global.mongo = require('mongoose').connect('mongodb://localhost/test'),
    route = require('./route');

app.configure(conf.general);
app.configure('development', conf.develop);

route.web();
route.socket();

server.listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});