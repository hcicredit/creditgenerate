module.exports = (
    function(){
        var jsdom = require('jsdom')
          , fs = require('fs')
          , conf = require('../conf')
          , init = init;

        init = function(){
            var window = global.window = jsdom.jsdom().createWindow();
            jsdom.jQueryify(window, conf.path.jquery, function(){
                var $ = global.$ = window.$;
            });

            fs.readFile(conf.path.templateXml, 'utf-8', function(err, template){
                console.log('Finish read xml file.');
                if(err) throw err;
                global.template = template = jsdom.jsdom(template);
                console.log('Finish parse xml file.');
            });
        };

        return {
            init: init
        };
    }
).call(this);
