module.exports = (function(){
    var express = require('express')
      , app = global.app;

    var general = function(){
	app.set('port', process.env.PORT || 3000);
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.cookieParser('scauhci'));
	app.use(express.session());
	app.use(app.router);
	app.use(express.static(__dirname + '/../public'));

        global.io.set('log level', 1);
    };

    var develop = function(){
	app.use(express.errorHandler());
    };

    var path = {
        docDir: __dirname + '/../public/doc'
    };

    return {
	general: general
      , develop: develop
      , path: path
    };
}).call(this);
