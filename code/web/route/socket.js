module.exports = (function(){
    var io = global.io,
	controller = require('../controllers');

    var socket = function(){
		io.sockets.on('connection', function(s){
		    console.log('Connect Successfully...');
			s.on('forward', controller.socket.forward);
			s.on('result', function(){
				console.log("Recieve.");
			});
		});
    };

    return socket;
}).call(this);
