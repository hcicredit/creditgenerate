module.exports = (function(){
    var web = require('./web'),
	socket = require('./socket');

    return {
	web: web,
	socket: socket
    };
}).call(this);
