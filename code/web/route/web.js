module.exports = (function(){
    var app = global.app,
        controller = require('../controllers');

    var web = function(){
        app.all('/api/model/business/:id?', function(req, res, next){
            var method = req.method;
            if(method === 'POST'){
                controller.business.add(req, res);
            }else if(method === 'GET'){
                controller.business.get(req, res);
            }else if(method === 'PUT'){
                controller.business.update(req, res);
            }else if(method === 'DELETE'){
                controller.business.del(req, res);
            };
        });
        app.post('/api/model/user/sign_in', controller.user.get);
        app.post('/api/model/user/sign_up', controller.user.add);
        app.put('/api/model/user', controller.user.update);
    };

    return web;
}).call(this);
