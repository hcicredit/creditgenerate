module.exports = (function(){
    var model = require('../../models'),
	post;

    post = function(req, res){
	var fn = function(result){
	    res.json(result);
	};
	model.mysql.query(req.body, fn);
    };

    return {
	post: post
    };
}).call(this);
