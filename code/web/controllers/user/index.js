module.exports = (
    function(){
        var model = require('../../models')
          , add
          , get
          , update
          , del;

        add = function(req, res){
            var user = req.body;
            user = new model.User(user);

            var fn = function(err, result){
                if(err){
                    res.statusCode = 400;
                    res.end();
                }else{
                    res.json(result);
                };
            };

            user.save(fn);
        };

        get = function(req, res, next){
            var user = req.body
              , name = user.name
              , password = user.password;

            var fn = function(err, user){
                if(err) throw err;

                if(user.auth(password)){
                    res.json(user);
                }else{
                    res.statusCode = 404;
                    res.end();
                };
            };

            model.User.findOne({name: name}, fn);
        };

        update = function(req, res){
            var user = req.body
              , id = user._id
              , name = user.name
              , password = user.password;

            var fn = function(err, user){
                if(err) throw err;

                if(user.auth(name)){
                    var fn = function(err, user){
                        if(err) throw err;
                    };
                    var user = req.body;
                    delete user._id;
                    model.User.findByIdAndUpdate(id, user, fn);
                }else{
                    res.json(null);
                };
            };

            model.User.findOne({name: name}, fn);
        };

        del = function(req, res){
            var id = req.params.id;

            var fn = function(err, result){
                if(err) throw err;
                res.json(result);
            };

            model.Business.findByIdAndRemove(id, fn);
        };

        return {
            add: add
          , get: get
          , update: update
          , del: del
        };
    }
).call(this);
