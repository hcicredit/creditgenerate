module.exports = (
    function(){
        var model = require('../../models')
          , add
          , get
          , update
          , del
          , generate;

        add = function(req, res){
            var business = req.body;
            business = new model.Business(business);

            var fn = function(err, result){
                if(err) throw err;
                res.json(result);
            };

            business.save(fn);
        };

        get = function(req, res){
            var id = req.params.id;

            var fn = function(err, result){
                if(err) throw err;
                res.json(result);
            };

            if(id === undefined){
                model.Business.find({}, fn);
            }else{
                model.Business.findById(id, fn);
            };
        };

        update = function(req, res){
            var business = req.body
              , id = business._id;

            delete business._id;

            var fn = function(err, result){
                if(err) throw err;
                res.json(result);
            };

            model.Business.findByIdAndUpdate(id, business, fn);
        };

        del = function(req, res){
            var id = req.params.id;

            var fn = function(err, result){
                if(err) throw err;
                res.json(result);
            };

            model.Business.findByIdAndRemove(id, fn);
        };

        generate = function(req, res){
            var id = req.params.id
              , io = global.io;
            var fn = function(result){
                res.json(result);
            };
            var cmd = {
                act: 'generate'
              , id : id
            };

            io.sockets.emit('cmd', cmd);
        };

        return {
            add: add
          , get: get
          , update: update
          , del: del
          , generate: generate
        };
    }
).call(this);
