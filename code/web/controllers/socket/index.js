module.exports = (function(){
    var forward,
	io = global.io;

    forward = function(data){
	io.sockets.emit('fromforward', data);
    };

    return {
	forward: forward
    };
}).call(this);